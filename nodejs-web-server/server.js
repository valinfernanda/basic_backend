const http = require("http");

const requestListener = (request, response) => {
  response.setHeader("Content-Type", "application/json");
  response.setHeader("X-Powered-By", "NodeJS");

  const { url, method } = request;

  if (url === "/") {
    if (method === "GET") {
      response.statusCode = 200;
      response.end(
        JSON.stringify({
          message: "Ini adalah homepage",
        })
      );
      // curl -X GET http://localhost:5000/
    } else {
      response.statusCode = 400;
      response.end(
        JSON.stringify({
          message: `Halaman tidak dapat diakses dengan ${method} request`,
        })
      );
      // curl -X <any> http://localhost:5000/
    }
  } else if (url === "/about") {
    if (method === "GET") {
      response.statusCode = 200;
      response.end(
        JSON.stringify({
          message: "Halo! Ini adalah halaman about",
        })
      );

      // curl -X GET http://localhost:5000/about
    } else if (url === "/about") {
      if (method === "GET") {
        response.end("<h1>Halo! Ini adalah halaman about</h1>");
        // curl -X GET http://localhost:5000/about
      } else if (method === "POST") {
        let body = [];

        request.on("data", (chunk) => {
          body.push(chunk);
        });

        request.on("end", () => {
          body = Buffer.concat(body).toString();
          const { name } = JSON.parse(body);
          response.statusCode = 200;
          response.end(
            JSON.stringify({
              message: `Halo, ${name}! Ini adalah halaman about`,
            })
          );
        });
        // curl -X POST http://localhost:5000/about
      } else {
        response.statusCode = 400;
        response.end(
          JSON.stringify({
            message: `Halaman tidak dapat diakses menggunakan ${method}, request`,
            // curl -X <any> http://localhost:5000/about
          })
        );
      }
    } else {
      response.statusCode = 404;
      response.end(
        JSON.stringify({
          message: "Halaman tidak ditemukan!",
        })
      );
    }

    // curl -X <any> http://localhost:5000/about
  } else {
    response.end(
      JSON.stringify({
        message: "Halaman tidak ditemukan!",
        //Karena response.end() menerima string (atau buffer), maka kita perlu mengubah objek JavaScript menjadi JSON string menggunakan JSON.stringify().
      })
    );
  }

  // curl -X <any> http://localhost:5000/<any>
};

const server = http.createServer(requestListener);

const port = 5000;
const host = "localhost";

server.listen(port, host, () => {
  console.log(`Server berjalan pada http://${host}:${port}`);
});

//curl -X GET http://localhost:5000/about -i

//curl -X GET http://localhost:5000/test -i

//curl -X DELETE http://localhost:5000/ -i

//http://localhost:5000
