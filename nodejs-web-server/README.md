npm init --y : --y berfungsi untuk menjawab seluruh pertanyaan yg diberikan NPM ketika membuat proyek baru dengan jawaban/nilai default.
npm run start 

curl -X GET http://localhost:5000/ : request pada server tersebut melalui cURL (di cmd)
bisa juga di browser : http://localhost:5000/

nah minusnya, hasilnya akan sama walopun kita tulis: 
curl -X POST http://localhost:5000
curl -X PUT http://localhost:5000
curl -X DELETE http://localhost:5000
ini semua karena kita belum kasih logika dalam menangani permintaan tersebut. 
notes: waktu nyoba pastikan HTTP Server sedang berjalan. kalo terhenti, jalanin lagi pake npm run start 

Error: listen EADDRINUSE: address already in use 127.0.0.1:5000 : berarti port 5000 sudah dipakai sebelumnya, jadi pake npx kill-port 5000(5000 ini sesuaikan sama portnya), baru npm run start lagi

Jika problemnya tidak tahu cara mematikan port namun tidak terjadi error pada terminal, maka tekan CTRL+C lalu pilih Y/y pada keyboard

HEADER 
header dengan properti yang tidak standar atau kalo kamu buat nama propertinya secara mandiri, maka sangat disarankan untuk menambahkan huruf X di awal nama propertinya. 
misal: response.setHeader('X-Powered-By', 'NodeJS');
notes: penulisan properti header dituliskan secara Proper Case atau setiap kata diawali dengan huruf kapital dan setiap katanya dipisahkan oleh tanda garis (-)
