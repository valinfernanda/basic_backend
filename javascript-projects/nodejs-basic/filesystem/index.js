// TODO: tampilkan teks pada notes.txt pada console.
//Tips:bisa gunakan method path.resolve(__dirname, 'notes.txt');
//Untuk mengakses berkas pada komputer kita dapat menggunakan method fs.readFile(). Method ini menerima tiga argumen yakni: lokasi berkas, encoding,
//dan callback function yang akan terpanggil bila berkas berhasil/gagal diakses.
//Fungsi readFile baik versi asynchronous ataupun synchronous, bekerja dengan membaca berkas hingga selesai sebelum mengembalikan data
const fs = require("fs");
const path = require("path");

const fileReadCallback = (error, data) => {
  if (error) {
    console.log("Gagal membaca berkas");
    return;
  }
  console.log(data);
};

fs.readFile(path.resolve(__dirname, "notes.txt"), "UTF-8", fileReadCallback);
