npm init -> untuk buat package.json

---UNTUK PAKE MODE REPL (READ, EVAL, PRINT LOOP)---
node -> untuk eksekusi js 1 baris

.editor -> untuk eksekusi lebih dari 1 baris
buat eksekusi .editor -> CTRL+D
buat keluar dari menu editor -> CTRL+C

.exit -> untuk keluar dari RUPL

Object.getOwnPropertyNames(global); -> jalanin ini didalam REPL buat liat member dari global object
process.memoryUsage() -> mendapatkan informasi tentang penggunaan CPU ketika proses berjalan.

---

properti process.env.PWD menyediakan informasi mengenai lokasi di mana proses dijalankan
process.env menyimpan nilai atau mendapatkan informasi mengenai environment yang digunakan selama proses sedang berlangsung.
properti process.env.USER menyimpan informasi nama user pada komputer Anda
process.argv menampung nilai baris perintah dalam bentuk array ketika menjalankan proses.

ekspor
module.exports = coffee;
module.exports = { firstName, lastName };
impor
const coffee = require('./coffee');
const { firstName, lastName } = require('./user');

running..
node ./modularization/index.js
npm run start-dev -> menjalankan script yg ada di berkas package.json

