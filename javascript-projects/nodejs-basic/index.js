const moment = require("moment");

const message = (name) => {
  console.log(`Hello ${name}`);
};

message("JavaScript");

const date = moment().format("MMM Do YY");
console.log(date);
