// Events
// Fungsi on menerima dua buah argumen, yang pertama adalah nama event dan yang kedua adalah listener atau fungsi yang akan dieksekusi ketika event terjadi.
// Cara membangkitkan suatu event? Setiap instance dari EventEmitter juga memiliki fungsi emit() yang berguna untuk membangkitkan event. Juga bisa mendaftarkan lebih dari satu fungsi listener pada sebuah event menggunakan fungsi on.

const { EventEmitter } = require("events"); // TODO 1

const birthdayEventListener = (name) => {
  console.log(`Happy birthday ${name}!`);
};

const myEmitter = new EventEmitter(); // TODO 2

// TODO 3
myEmitter.on("birthday", birthdayEventListener);

// TODO 4
myEmitter.emit("birthday", "Valin");

// TODO 1 : Buat atau impor variabel EventEmitter dari core module events.
// TODO 2 : Buat variabel myEmitter yang merupakan instance dari EventEmitter.
// TODO 3 : Tentukan birthdayEventListener sebagai aksi ketika event ‘birthday’ dibangkitkan pada myEmitter.
// TODO 4 : Bangkitkanlah event ‘birthday’ pada myEmitter dengan method emit() dan beri nilai argumen listener dengan nama Anda.
